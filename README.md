Provee el entorno de desarrollo para php-7.4, nginx, mysql, phpmyadmin por medio de Docker.

## Instalación

* Instalar *Docker Engine* siguiento las [instrucciones oficiales](https://docs.docker.com/engine/installation/).

* Si está en Linux instalar *Docker Composer* siguiendo las [instrucciones oficiales](https://docs.docker.com/compose/install/) (para Mac y Windows al instalar el *Engine* se incluye el *Composer*).

* Conar este repositorio:

```
~$ git clone https://gitlab.com/desarrollo-de-aplicaciones-web-2/lemp-docker-compose.git
```

* Crear branch para la configuración personal (ver instrucciones de configuración abajo).

* Iniciar la herramienta: *docker-compose up*

```
$ cd lemp-docker-compose
$ docker-compose up
```

Si se quiere ejecutar en background:

```
$ cd lemp-docker-compose
$ docker-compose up -d
```

## Configuración

Los contenedores se pueden configurar declarando variables en [el archivo .env en la carpeta del proyecto](https://docs.docker.com/compose/env-file/).

Se recomienda crear un branch personal para la configuración:

```
$ cd lemp-docker-compose
$ git checkout -b nueva_rama
$ vim .env
```

Luego, para actualizar los cambios sin cambiar de branch (y sin perder la configuración personal):

```
$ cd lemp-docker-compose
$ git pull origin master
```

### Variables

Las variables que se pueden configurar se pueden ver facilmente en el archivo *docker-compose.yml* donde se
definen con sus valores por defecto.

* **MYSQLDBCONT_DATA_DIR** Carpeta donde se almacenan las bases de datos.
* **CONT_PROJECTS_DIR** Carpeta en la que están los proyectos
* **CONT_PORT** Puerto en el que se expone el servicio PHP/CONT_PORT** Puerto en el que se expone el servicio PHP/NGINX.

## Ver Logs

Cuando se ejecuta *docker-compose up* se pueden ver los log (NGINX, PHP, MariaDB) en la misma terminal en la que se ejecutó. Si se ejecutó en background con *docker-compose up -d* se pueden ver los logs con el comando:

```
$ docker-compose logs
```

Si se desea que siga mostrando los logs hasta presionar *Ctrl-C*:

```
$ docker-compose logs -f
```

O también se puede ver el log de cada contenedor por separado:

```
$ docker logs www
$ docker logs mysql
```

## Conexión a la DB

### Desde el contenedor www

El contenedor www puede conectarse al contenedor mariadb por nombre. Así que en todos los *settings.php* de los
proyectos se debe poner como servidor de base de datos *mysql*.

### Desde el equipo

El contenedor mysql expone el puerto de mysql: 3306 (por defecto). Cualquier herramienta para administración
de bases de datos MySQL funcionará conectandola a localhost:3306.

La herramienta *mysql* por defecto viene configurada para conectarse a una DB local por sockets unix. Para que funcione se le debe dar la IP de localhost (usar el nombre localhost no me funciona):

```
$ mysql -h 127.0.0.1 dbname < file.sql
```

## Terminal

```
$ docker exec -ti www su -l www-data
```

Para conveniencia se provee el un script:

```
$ cd lemp-docker-compose
$ ./cont-terminal.sh
```

Dentro del contenedor los proyectos están en */var/www/html* :

```
$ ./cont-terminal.sh
$ cd /var/www/html/prj
```

## XDebug

El PHP en el contenedor www tiene activado XDebug y configurado para conectarse a la IP que haga el query, así que
cualquier editor debe funcionar correctamente.

Ver [*With an unknown IP/multiple developers* en el manual de XDebug](https://xdebug.org/docs/remote).

### En Mac

Por defecto Docker Engine en Mac no permite que los contenedores se conecten al host. Para que XDebug funcione
se debe ejecutar el script *mac-net-alias.sh* incluido en este repositorio y cambiar en el archivo
*php-config/999-aaa-local.ini* las siguientes lineas:

```
xdebug.remote_connect_back = 0
xdebug.remote_host = 10.254.254.254
```

Recuerde configurar el mapeo de rutas (Path Maps) en la configuración de Xdebug de su editor/IDE, el cual permitirá al editor relacionar la ruta raíz del contenedor /var/www/html con la raíz de sus archivos www (la que puso en la variable CONT_PROJECTS_DIR).

## Más Información

Para más información [RTFM](https://docs.docker.com).
